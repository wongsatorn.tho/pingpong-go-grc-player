package server

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"google.golang.org/grpc"
)

// RequestIdInjectServerInterceptor returns a new unary server interceptor for inject unique request-id on all requests.
func RequestIdInjectServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (_ interface{}, err error) {
		uuid := fmt.Sprintf("%s_%s_%s",
			time.Now().Format("20060102"),
			time.Now().Format("030405"),
			uuid.New().String(),
		)
		grpc_ctxtags.Extract(ctx).Set("grpc.request_id", uuid)
		return handler(ctx, req)
	}
}
