package service

import (
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/wongsatorn.tho/pingpong-go-grc-player/pkg/proto/tagthai/service"
	pb "gitlab.com/wongsatorn.tho/pingpong-go-grc-player/pkg/proto/tagthai/service"
	"gitlab.com/wongsatorn.tho/pingpong-go-grc-player/pkg/utils"

	"github.com/go-redis/redis/v8"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type MatchRepo interface {
	AddMatchLog(ctx context.Context, matchLogs []service.Match) error
	GetMatchById(ctx context.Context, matchId string) ([]*service.Match, error)
}

type PlayerServiceServerImpl struct {
	pb.PlayerServiceServer
	repo MatchRepo
}

//NewPlayerServiceServerImpl(Configs)
func NewPlayerServiceServerImpl() pb.PlayerServiceServer {
	return &PlayerServiceServerImpl{
		repo: NewRepo(),
	}
}

func (s *PlayerServiceServerImpl) NewMatch(c context.Context, e *emptypb.Empty) (*emptypb.Empty, error) {
	var wg sync.WaitGroup
	var matchLogs []service.Match
	var matchId int

	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "MoUpg0PPoN",
		DB:       0, // use default DB
	})

	lastMatchIdString, err := redisClient.Get(c, "match_id").Result()
	if err == redis.Nil {
		matchId = 1
	} else if err != nil {
		log.Fatal(err)
	} else {
		lastMatchId, err := strconv.Atoi(lastMatchIdString)
		if err != nil {
			log.Fatal(err)
		}
		matchId = lastMatchId + 1
	}

	matchChannel := make(chan service.Match)
	defer close(matchChannel)
	logChannel := make(chan service.Match)
	defer close(logChannel)

	wg.Add(3)
	go Player("A", true, matchChannel, logChannel, &wg)
	go Player("B", false, matchChannel, logChannel, &wg)

	go WriteLog(matchId, logChannel, &wg, &matchLogs)
	wg.Wait()

	err = s.repo.AddMatchLog(c, matchLogs)

	if err != nil {
		log.Fatal(err)
	}

	p, err := json.Marshal(matchLogs)
	if err != nil {
		return e, err
	}

	err = redisClient.Set(c, "match_id", matchId, 0).Err()
	if err != nil {
		return e, err
	}

	err = redisClient.Set(c, "match", p, 0).Err()
	if err != nil {
		return e, err
	}

	return e, nil
}

func (s *PlayerServiceServerImpl) GetMatchById(c context.Context, matchId *service.MatchId) (*service.MatchLogs, error) {
	data, err := s.repo.GetMatchById(c, matchId.MatchId)

	fmt.Println(data)
	if err != nil {
		log.Fatal(err)
	}

	return &service.MatchLogs{Match: data}, err
}

func (s *PlayerServiceServerImpl) GetLastMatch(c context.Context, e *emptypb.Empty) (*service.MatchLogs, error) {
	var matchLogs []*service.Match
	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "MoUpg0PPoN",
		DB:       0, // use default DB
	})

	matchJSON, err := redisClient.Get(c, "match").Result()

	if err == redis.Nil {
		return nil, err
	} else if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal([]byte(matchJSON), &matchLogs)
	if err != nil {
		log.Fatal(err)
	}

	return &service.MatchLogs{Match: matchLogs}, err
}

func Player(playerName string, isServe bool, matchChannel chan service.Match, logChannel chan service.Match, wg *sync.WaitGroup) {
	defer wg.Done()

	if isServe {
		servedBallPower := utils.Serve()
		fmt.Printf("Serve %s: %f\n", playerName, servedBallPower)

		m := service.Match{
			Turn:             1,
			WakingProcess:    playerName,
			BallPowerHit:     servedBallPower,
			BallPowerReceive: 0,
			GoRoutineNumber:  int32(goid()),
			Time:             timestamppb.Now(),
		}
		matchChannel <- m
		logChannel <- m
	}

	for {
		rm := <-matchChannel
		returnedBallPower := Table(rm.BallPowerHit)
		fmt.Printf("Return %s: %f\n", playerName, returnedBallPower)

		if rm.IsEndMatch {
			fmt.Printf("%s Win\n", playerName)
			break
		}

		servedBallPower := utils.Serve()
		fmt.Printf("Serve %s: %f\n", playerName, servedBallPower)

		if servedBallPower <= returnedBallPower {
			fmt.Printf("%s Lose\n", playerName)
			m := service.Match{
				Turn:             rm.Turn + 1,
				WakingProcess:    playerName,
				BallPowerReceive: returnedBallPower,
				BallPowerHit:     servedBallPower,
				GoRoutineNumber:  int32(goid()),
				Time:             timestamppb.Now(),
				IsEndMatch:       true,
			}
			matchChannel <- m
			logChannel <- m
			break
		}

		m := service.Match{
			Turn:             rm.Turn + 1,
			WakingProcess:    playerName,
			BallPowerReceive: returnedBallPower,
			BallPowerHit:     servedBallPower,
			GoRoutineNumber:  int32(goid()),
			Time:             timestamppb.Now(),
		}
		matchChannel <- m
		logChannel <- m
	}

	fmt.Printf("%s End\n", playerName)
}

func WriteLog(matchId int, logChannel chan service.Match, wg *sync.WaitGroup, matchLogs *[]service.Match) {
	defer wg.Done()

	f, err := os.Create("users.csv")

	if err != nil {
		log.Fatalln("failed to open file", err)
	}
	defer f.Close()

	for v := range logChannel {
		v.MatchNumber = int32(matchId)
		*matchLogs = append(*matchLogs, v)
		fmt.Println(v)

		w := csv.NewWriter(f)
		err = w.Write([]string{
			fmt.Sprintf("%d", v.MatchNumber),
			fmt.Sprintf("%d", v.Turn),
			v.WakingProcess,
			fmt.Sprintf("%f", v.BallPowerReceive),
			fmt.Sprintf("%f", v.BallPowerHit),
			fmt.Sprintf("%d", v.GoRoutineNumber),
			v.Time.String(),
		})

		if err != nil {
			log.Fatal(err)
		}
		w.Flush()

		if v.IsEndMatch {
			break
		}
	}

}

func Table(ballPower float64) float64 {
	var request struct {
		BallPower float64 `json:"ball_power"`
	}

	var response struct {
		BallPower float64 `json:"ballPower"`
	}

	request.BallPower = ballPower

	p, err := json.Marshal(request)
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	//config baseUrl + servicePath
	resp, err := http.Post("http://localhost:8889/api/v1/table", "application/json", bytes.NewBuffer(p))
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(body, &response)
	if err != nil {
		panic(err.Error())
	}

	if response.BallPower == 0 {
		panic("Ball power should not be 0")
	}

	return response.BallPower
}

func goid() int {
	var buf [64]byte
	n := runtime.Stack(buf[:], false)
	idField := strings.Fields(strings.TrimPrefix(string(buf[:n]), "goroutine "))[0]
	id, err := strconv.Atoi(idField)
	if err != nil {
		panic(fmt.Sprintf("cannot get goroutine id: %v", err))
	}
	return id
}
