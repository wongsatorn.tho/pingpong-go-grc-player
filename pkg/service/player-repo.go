package service

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/wongsatorn.tho/pingpong-go-grc-player/pkg/proto/tagthai/service"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Repo struct {
	repo *mongo.Database
}

func bindingClient() (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	conn := fmt.Sprintf(
		"mongodb://%s:%s@localhost/%s?retryWrites=true&w=majority",
		"user",
		"1234",
		"pingpong",
	)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(conn))
	if err != nil {
		return nil, err
	}

	// check connection
	if err := client.Ping(context.TODO(), nil); err != nil {
		return nil, errors.Wrapf(err, "ping failed")
	}

	return client, nil
}

func NewRepo() *Repo {
	client, err := bindingClient()
	if err != nil {
		panic(err)
	}

	return &Repo{
		repo: client.Database("pingpong"),
	}
}

func (r Repo) AddMatchLog(ctx context.Context, matchLogs []service.Match) error {

	coll := r.repo.Collection("match")

	var matchLogsInterface []interface{}

	for _, v := range matchLogs {
		fmt.Println(matchLogsInterface)
		matchLogsInterface = append(matchLogsInterface, v)
	}

	matchResult, err := coll.InsertMany(ctx, matchLogsInterface)

	if err != nil {
		return err
	}

	fmt.Printf("Inserted %v documents into match collection!\n", len(matchResult.InsertedIDs))

	return nil
}

func (r Repo) GetMatchById(ctx context.Context, matchId string) ([]*service.Match, error) {

	matchNumber, err := strconv.Atoi(matchId)
	if err != nil {
		return nil, err
	}

	filter := bson.M{
		"matchnumber": matchNumber,
	}

	coll := r.repo.Collection("match")
	cur, err := coll.Find(ctx, filter, options.Find())
	if err != nil {
		return nil, err
	}

	var rtn []*service.Match
	if err := cur.All(ctx, &rtn); err != nil {
		return nil, err
	}
	return rtn, nil
}
